from django.db import models

class Perso(models.Model):
    init = models.IntegerField()
    action = models.PositiveIntegerField()
    name = models.CharField(max_length=250)
    photo = models.ImageField(blank=True, null=True)

    def __unicode__(self):
        return unicode(self.name)

class Guerre(models.Model):
    persos = models.ManyToManyField(Perso)
    name = models.CharField(max_length=250)

    def __unicode__(self):
        return unicode(self.name)


class Bataille(Guerre):

    def __unicode__(self):
        return unicode(self.name)


class Round(Bataille):
    bataille = models.ForeignKey(Bataille, related_name='rounds')
    number = models.PositiveIntegerField(default=1)

    class Meta:
        unique_together = ("bataille", "number")
