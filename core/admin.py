from django.contrib import admin
from core.models import Perso, Bataille, Round


for model in [Perso, Bataille, Round]:
    admin.site.register(model)
# Register your models here.
