# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='bataille',
            name='name',
            field=models.CharField(default='prout', max_length=250),
            preserve_default=False,
        ),
    ]
