from django.shortcuts import render

# Create your views here.
from django.views.generic import ListView

from core.models import Guerre


class ListGuerreView(ListView):

    model = Guerre
    template_name = 'guerre_list.html'

guerre_list = ListGuerreView.as_view()
