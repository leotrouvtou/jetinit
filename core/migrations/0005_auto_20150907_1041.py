# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_auto_20150904_1655'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='round',
            name='id',
        ),
        migrations.AddField(
            model_name='round',
            name='bataille_ptr',
            field=models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, default='1', serialize=False, to='core.Bataille'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='round',
            name='bataille',
            field=models.ForeignKey(related_name='rounds', to='core.Bataille'),
        ),
    ]
