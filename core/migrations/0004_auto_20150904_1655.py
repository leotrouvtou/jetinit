# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20150904_1647'),
    ]

    operations = [
        migrations.CreateModel(
            name='Guerre',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=250)),
                ('persos', models.ManyToManyField(to='core.Perso')),
            ],
        ),
        migrations.RemoveField(
            model_name='bataille',
            name='id',
        ),
        migrations.RemoveField(
            model_name='bataille',
            name='name',
        ),
        migrations.RemoveField(
            model_name='bataille',
            name='persos',
        ),
        migrations.AddField(
            model_name='bataille',
            name='guerre_ptr',
            field=models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, default='1', serialize=False, to='core.Guerre'),
            preserve_default=False,
        ),
    ]
